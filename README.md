# PalaBuildTool

![image info](./PalaBuildTool.png)

Un simple utilitaire pour directement build son mod et le convertir en .pala

Voici un exemple du fichier config.yml 

```yml
input: "build/libs/Fichier.jar" #Fichier .jar ou .pala
output: "build/libs/" #Dossier ou il y aura le fichier converti en .pala ou .jar
mode: "1" #Il y à 2 mode, le mode 1 pour convertir de .jar > .pala, et le mod pour convertir de .pala > .jar
build: true #Si vous utilisez cet utilitaire dans le workspace de votre mod et que c'est à true il éxécutera automatiquement le gradlew build
```
