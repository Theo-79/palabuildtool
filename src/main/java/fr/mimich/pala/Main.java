package fr.mimich.pala;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;

//Author : https://github.com/mimich-FR
public final class Main {

    public static final void reverse(final String inputPath, final String outpoutPath, int mode){

        System.out.println("Lancement...");
        File in, out;
        in = new File(inputPath);
        out = new File(outpoutPath);
        System.out.println("Chargement du fichier : " + in + "...");
        final Set<File> files = new HashSet<>();
        if (in.isDirectory()){
            for (int i = 0; i < in.listFiles().length ; i++) {
                final File file = in.listFiles()[i];
                if ((mode == 1 && file.getName().endsWith(".jar")) || (mode == 2 && file.getName().endsWith(".pala"))) {
                    files.add(file);
                    System.out.println("(" + files.size() + "/" + in.listFiles().length + ") " + file.getName() + "ne peut être inversé !");
                }
            }
        } else {
            files.add(in);
        }
        
        System.out.println("Les fichiers ont été chargés ! (" + files.size() + ")");
        for (File file : files){
            try {
                Files.write(new File(out, file.getName().split("\\.")[0] + (mode == 1 ? ".pala" : ".jar")).toPath(), cipher(file, mode));
                System.out.println(file.getName() + " à été converti. Enjoy !");
            } catch (IOException | IllegalBlockSizeException | BadPaddingException | NoSuchPaddingException | InvalidKeyException | NoSuchAlgorithmException e) {
                e.printStackTrace();
                System.err.println("Erreur !");
                System.exit(1);
            }
        }
    }

    private static byte[] cipher(final File file, int mode) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, IOException, BadPaddingException, IllegalBlockSizeException {
    	//get this key from a web request on a paladium server.
		final String KEY = "dK5rcm5RuMBETH5C";
		final SecretKeySpec secretKey = new SecretKeySpec(KEY.getBytes(), "AES");
		final Cipher cipher = Cipher.getInstance("AES");
		cipher.init(mode, secretKey);
		FileInputStream inputStream = new FileInputStream(file);
		byte[] inputBytes = new byte[(int) file.length()];
		inputStream.read(inputBytes);
		byte[] outputBytes = cipher.doFinal(inputBytes);
		inputStream.close();
		return outputBytes;
    }
}