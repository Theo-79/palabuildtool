package fr.theo79.buildtool;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.github.jsixface.YamlConfig;

import fr.mimich.pala.Main;

public class Build {

	public static void main(String[] args) throws IOException, InterruptedException {
		InputStream resource = new FileInputStream("config.yml");
		YamlConfig config = YamlConfig.load(resource);
		String input = config.getString("input");
		String output = config.getString("output");
		String mode = config.getString("mode");
		String build = config.getString("build");
		if (build == "true" || Boolean.parseBoolean(build) == true) {
			ProcessBuilder processBuilder = new ProcessBuilder();
			processBuilder.command("cmd.exe", "/c", "gradlew build");
		    Process process = processBuilder.start();
		    BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		    String line;
		    while ((line = reader.readLine()) != null) {
		        System.out.println(line);
		    }
		    int exitCode = process.waitFor();
		    System.out.println("\nExited with error code : " + exitCode);
		}
	    Main.reverse(input, output, Integer.parseInt(mode));
	}
	
}
